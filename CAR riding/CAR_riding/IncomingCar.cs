﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace GhostRiding
{
    class IncomingCar : Game1
    {
        private int xPos;
        public int yPox = -3000;
        private int snelheid;
        private Texture2D incomingCar;
        private static Random rndGen;
        public bool isCrashed = false;
        private bool autoOpSnelweg = false;
        private List<string> cars = new List<string>(new string[] {"BlueCar","GrayCar","OrangeCar","PinkCar","PurpleCar","RedCar","TurquoiseCar","WhiteCar","YellowCar"});
        
        public IncomingCar(ContentManager Content,int xPos)
        {
            rndGen = new Random();
            this.Content = Content;
            this.xPos = xPos;
        }
        //Update() komt in Update() te staan om de auto te laten bewegen.
        public void Update(int xPosPion)
        {
            if (autoOpSnelweg == false)//Als de auto niet op de weg rijd...
            {
                snelheid = rndGen.Next(2, 7);//De snelheid van de auto wordt random bepaalt, 2 of 3
                yPox = rndGen.Next(-2000, -100);//Zorgt dat de auto's niet allemaal op 1 lijn komen aanrijden
                
                incomingCar = Content.Load<Texture2D>(cars[rndGen.Next(0, 9)]);//Random auto
                autoOpSnelweg = true;//De auto is op de weg
            }
            else
            {
                yPox += snelheid;//De yPositie van de auto stijgt met 2 of 3
                if ((yPox + 50 >= base.graphics.PreferredBackBufferHeight - 150) && (yPox - 50 <= base.graphics.PreferredBackBufferHeight - 80))
                {
                    if ((xPosPion >= xPos) && (xPosPion <= xPos + 50) || ((xPos >= xPosPion) && (xPos <= xPosPion + 50)))
                    {
                        isCrashed = true;
                    }
                }
                if (yPox >= base.graphics.PreferredBackBufferHeight) //Als de auto het venster uit rijdt...
                {
                    autoOpSnelweg = false; //De auto is van de snelweg.
                }
            }
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(incomingCar, new Rectangle(xPos, yPox, 50, 100), Color.White);//Tekend de auto.
        }
    }
}






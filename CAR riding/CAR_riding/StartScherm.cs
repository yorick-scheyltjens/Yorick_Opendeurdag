﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
namespace GhostRiding
{
    class StartScherm : Game1
    {
        private Texture2D startScherm;
        private SpriteFont SpriteFont;
        public StartScherm(ContentManager Content)
        {
            this.Content = Content;
        }
        public void Load()
        {
            startScherm = Content.Load<Texture2D>("StartScherm");
            SpriteFont = Content.Load<SpriteFont>("SpriteFont");
        }
        public void Update()
        {

        }
        public void Draw(SpriteBatch spriteBatch)
        {
            Color fontColor = new Color(0,155,255,255);
            spriteBatch.Draw(startScherm,new Rectangle(0,0, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight), Color.White);
            spriteBatch.DrawString(SpriteFont, "Press 'Space' to start", new Vector2(40,350), fontColor);
            spriteBatch.DrawString(SpriteFont, "Press 'Q' to quit", new Vector2(75,440),fontColor);
        }
    }
}

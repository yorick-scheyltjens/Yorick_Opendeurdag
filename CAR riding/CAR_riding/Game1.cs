using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Timers;

namespace GhostRiding
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        public GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        DrawAllObjects DrawAllObjects;
        private static int score = 0;
        public int Score
        {
            get
            {
                return score;
            }
            set
            {
                score = value;
            }
        }
        //Initialize() wordt uitgevoert nog voor je project is geladen om bepaalde zaken klaar te zetten.
        protected override void Initialize()
        {
            base.Initialize();
        }
        //Game1()- Hierin staat hoe je programma eruit ziet moet zien(hoogte, breedte, snelheid van je Update(). 
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            this.Window.Title = "GhostRiding";
            graphics.IsFullScreen = false;
            graphics.PreferredBackBufferHeight = 960; //Hoogte van je formulier
            graphics.PreferredBackBufferWidth = 440; //Breedte van je formulier.
            IsFixedTimeStep = true;
            TargetElapsedTime = TimeSpan.FromMilliseconds(5); //Snelheid van je Update() in miliseconden. 
        }


        //LoadContent() wordt tijdens het laden uitgevoerd(Klaarzetten van de foto's).
        protected override void LoadContent()
        {
            DrawAllObjects = new DrawAllObjects(Content);
            spriteBatch = new SpriteBatch(GraphicsDevice);
            DrawAllObjects.Load();
        }
        protected override void UnloadContent() { } //UnloadContent wordt niet gebruikt.
        //De Update() wordt in dit geval om de 4 miliseconden uitgevoert, zie Game1().
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();
            DrawAllObjects.Update();
            if (DrawAllObjects.StartSchermAanwezig == false)
            {
                if (DrawAllObjects.isCrashed == false)
                {
                    Score++;
                }
                else
                {
                    IsMouseVisible = true;
                }
                this.Window.Title = "GhostRiding - score: " + score.ToString();
            }
            base.Update(gameTime);
        }
        //Draw() - Het projecteren van zaken.
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.DarkGray); //Donker grijze achtergrond.
            spriteBatch.Begin();//Met spriteBatch kan je objecten weergeven op je formulier.
            DrawAllObjects.Draw(spriteBatch);
            spriteBatch.End();//Eindig 
            base.Draw(gameTime);
        }
    }
}

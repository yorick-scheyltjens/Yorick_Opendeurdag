﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace GhostRiding
{
    class CrashedScreen : Game1
    {
        Texture2D crashedScreen;
        private SpriteFont SpriteFont;
        public CrashedScreen(ContentManager Content)
        {
            this.Content = Content;
        }
        public void Load()
        {
            crashedScreen = Content.Load<Texture2D>("CrashedScreen");
            SpriteFont = Content.Load<SpriteFont>("SpriteFont");
        }
        public void Update()
        {

        }
        public void Draw(SpriteBatch spriteBatch)
        {
            Color fontColor = new Color(0, 155, 255, 255);
            spriteBatch.Draw(crashedScreen, new Rectangle(0, 0, graphics.PreferredBackBufferWidth,graphics.PreferredBackBufferHeight), Color.White);
            spriteBatch.DrawString(SpriteFont, "Score: " + base.Score, new Vector2(170, 300), fontColor);
            spriteBatch.DrawString(SpriteFont, "Press 'Space' to restart", new Vector2(35, 350), fontColor);
            spriteBatch.DrawString(SpriteFont, "Press 'Q' to quit", new Vector2(73, 400), fontColor);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Timers;

namespace GhostRiding
{
    class OwnCar : Game1
    {
        private Texture2D ownCar;
        public int xPos = 195;
        private const int breedteRijvak = 85;


        public int GetxPos
        {
            get { return this.xPos; }
        }
        public OwnCar(ContentManager Content)
        {
            this.Content = Content;
        }
        public void Load()
        {

            this.ownCar = this.Content.Load<Texture2D>("OwnCar");
        }
        private bool wasKeyRightPressed = false;
        private bool wasKeyLeftPressed = false;
        public void Update()
        {
                KeyboardState state = Keyboard.GetState();
                if ((xPos - 1) != 24)
                {
                    if (state.IsKeyDown(Keys.Left))
                    {
                        wasKeyLeftPressed = true;
                        wasKeyRightPressed = false;
                    }
                }
                if ((xPos + 1) != 366)
                {
                    if (state.IsKeyDown(Keys.Right))
                    {
                        wasKeyRightPressed = true;
                        wasKeyLeftPressed = false;
                    }
                }
                if (wasKeyLeftPressed == true) 
                {
                     xPos--;
                     if((xPos != 25) && (xPos != 110) && (xPos != 195) && (xPos != 280) && (xPos != 365))
                     {
                         xPos--;
                     }
                     if ((xPos != 25) && (xPos != 110) && (xPos != 195) && (xPos != 280) && (xPos != 365))
                     {
                         xPos--;
                     }
                }
                if (wasKeyRightPressed == true) 
                {
                    xPos++;
                    if ((xPos != 25) && (xPos != 110) && (xPos != 195) && (xPos != 280) && (xPos != 365))
                    {
                        xPos++;
                    }
                    if ((xPos != 25) && (xPos != 110) && (xPos != 195) && (xPos != 280) && (xPos != 365))
                    {
                        xPos++;
                    }
                }
            

              if((xPos == 25) || (xPos == 110) || (xPos == 195) || (xPos == 280) || (xPos == 365))
              {
                    wasKeyRightPressed = false;
                    wasKeyLeftPressed = false;
              }
        }
        
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(ownCar, new Rectangle(xPos, base.graphics.PreferredBackBufferHeight - 130, 50, 100), Color.Green);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace GhostRiding
{
    class BackGround : Game1
    {
        private int yPosSnelweg1 = 0;
        private int yPosSnelweg2;
        private Texture2D snelweg;
        public BackGround(ContentManager Content)
        {
            this.Content = Content;
            yPosSnelweg2 = graphics.PreferredBackBufferHeight * -1;
            this.snelweg = this.Content.Load<Texture2D>("Snelweg");
        }
        public void Update()
        {
            yPosSnelweg1 += 1;
            yPosSnelweg2 += 1;
            if (yPosSnelweg1 == graphics.PreferredBackBufferHeight)
            {
                yPosSnelweg1 = graphics.PreferredBackBufferHeight * -1;
            }
            if (yPosSnelweg2 == graphics.PreferredBackBufferHeight)
            {
                yPosSnelweg2 = graphics.PreferredBackBufferHeight * -1;
            }
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(snelweg, new Rectangle(0, yPosSnelweg1, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight), Color.Gray);
            spriteBatch.Draw(snelweg, new Rectangle(0, yPosSnelweg2, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight), Color.Gray);
        }
    }
}

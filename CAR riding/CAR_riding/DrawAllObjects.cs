﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace GhostRiding
{
    class DrawAllObjects : Game1
    {
        IncomingCar object1;
        IncomingCar object2;
        IncomingCar object3;
        IncomingCar object4;
        IncomingCar object5;
        OwnCar ownCar;
        BackGround background;
        CrashedScreen crashedScreen;
        HighscoreForm highscoreForm;
        StartScherm startScherm;
        public bool isCrashed;
        public bool StartSchermAanwezig;
        public DrawAllObjects(ContentManager Content)
        {
            this.Content = Content;
        }
        public void Load()
        {
            object1 = new IncomingCar(Content, 25);
            object2 = new IncomingCar(Content, 110);
            object3 = new IncomingCar(Content, 195);
            object4 = new IncomingCar(Content, 280);
            object5 = new IncomingCar(Content, 365);

            ownCar = new OwnCar(Content);
            ownCar.Load();

            background = new BackGround(Content);

            crashedScreen = new CrashedScreen(Content);
            crashedScreen.Load();

            highscoreForm = new HighscoreForm();
            startScherm = new StartScherm(Content);
            startScherm.Load();
            isCrashed = false;
            StartSchermAanwezig = true;
        }
        public void Update()
        {
            KeyboardState state = Keyboard.GetState();
            if (StartSchermAanwezig == true)
            {
                if(state.IsKeyDown(Keys.Space))
                {
                    StartSchermAanwezig = false;
                }
                if(state.IsKeyDown(Keys.Q))
                {
                    Environment.Exit(0);
                }
            }
            else
            {
                object1.Update(ownCar.GetxPos);
                object2.Update(ownCar.GetxPos);
                object3.Update(ownCar.GetxPos);
                object4.Update(ownCar.GetxPos);
                object5.Update(ownCar.GetxPos);
                ownCar.Update();
                background.Update();
                if ((object1.isCrashed || object2.isCrashed || object3.isCrashed || object4.isCrashed || object5.isCrashed) == true) { isCrashed = true; }



                //Commando's van het crashedscreen omdat een class maar van 1 class kan overerven
                if (isCrashed == true)
                {
                    
                    if (state.IsKeyDown(Keys.Space))
                    {
                        isCrashed = false;
                        Score = 0;
                        object1.isCrashed = false;
                        object2.isCrashed = false;
                        object3.isCrashed = false;
                        object4.isCrashed = false;
                        object5.isCrashed = false;

                        object1.yPox = -3000;
                        object2.yPox = -3000;
                        object3.yPox = -3000;
                        object4.yPox = -3000;
                        object5.yPox = -3000;

                        highscoreForm.Close();

                        highscoreForm = new HighscoreForm();

                        ownCar.xPos = 195;


                    }
                    if (state.IsKeyDown(Keys.Q))
                    {
                        Environment.Exit(0);
                    }
                }
            }
            

        }
        public void Draw(SpriteBatch spriteBatch)
        {
            if (StartSchermAanwezig == true)
            {
                startScherm.Draw(spriteBatch);
            }
            else
            {
                if (isCrashed == false)
                {
                    background.Draw(spriteBatch);
                    object1.Draw(spriteBatch);
                    object2.Draw(spriteBatch);
                    object3.Draw(spriteBatch);
                    object4.Draw(spriteBatch);
                    object5.Draw(spriteBatch);
                    ownCar.Draw(spriteBatch);
                }
                else
                {
                    crashedScreen.Draw(spriteBatch);
                    highscoreForm.Show();
                    IsMouseVisible = true;
                }
            }
        }
    }
}
